TLDR; Backup the config, edit it and upload it to the FritzBox again.

Prequisites when using emojies or other more complicated unicode stuff:
- Set some setting to that unicode string and put something searchable ascii in front or behind it
  A new Wifi profile name for example
- Backup the config and search for the searchable ascii, there should also be the encoded unicode string which one can just past and copy elsewhere in the downloaded config
  For example, 🪴 turns into `\\360\\237\\252\\264`

How to:
- Backup the FritzBox config
- Load it into https://mengelke.de/Projekte/FritzBox-JSTool
- Change the SSIDs how ever you like
- Recalculate the checksum
- Download
- Upload the config to the FritzBox, the "select what to import" setting can be nice to see what actually changed
- :tada:
